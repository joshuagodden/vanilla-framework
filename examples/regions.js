class ExampleRegionMain extends VanillaFramework.Modules.View {
  constructor() {
    super();
    this.events = {
      '#add-button': {
        click: this.handleAddStudent.bind(this),
      },
    };
  }

  handleAddStudent() {
    
  }

  get tag() {
    return 'fieldset';
  }

  get template() {
    return `
      <legend>Greetings loyal student of Hogwarts! Please enter your details:</legend>
      <form>
        <label for="hogwarts-student-name">
          Name
          <input id="hogwarts-student-name" type="text" />
        </label>
        <label for="hogwarts-student-house">
          House
          <select id="hogwarts-student-house">
            <option>Griffindor</option>
            <option>Slytherin</option>
            <option>Hufflepuff</option>
            <option>Ravenclaw</option>
          </select>
        </label>
        <button id="add-button" type="button">Add</button>
      </form>
    `;
  }
}

class ExampleRegions extends VanillaFramework.Modules.View {
  constructor() {
    super({ students: [] });
    this.ui = {
      main: '#main',
    };

    this.regions = {
      main: '#main',
    };
  }

  get template() {
    return `
      <article id="main">
      </article>
      <article id="nav">
      </article>
    `;
  }

  get tag() {
    return 'section';
  }

  onAfterRender() {
    this.regions.main.render(new ExampleRegionMain());
  }
}


VanillaFramework.Application.render('#app', new ExampleRegions());