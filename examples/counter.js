class CounterView extends VanillaFramework.Modules.View {
  constructor(options) {
    super(options);
    this.model.on('change:counter', this.handleCounterChange.bind(this));
  }

  get template() {
    return `
      <fieldset>
        <legend>Counter example</legend>
        <span>Counter: </span><span data-bind="counter"></span>
        <button id="counter-plus-button" type="button">+</button>
        <button id="counter-minus-button" type="button">-</button>
        <br />
        <span>Check the console to see model binding!</span>
      </fieldset>
    `;
  }

  get tag() {
    return 'section';
  }

  get attributes() {
    return {
      'id': 'counter-button-example',
    };
  }

  get events() {
    return {
      '#counter-plus-button': {
        click: this.handleCounterPlusClick.bind(this),
      },
      '#counter-minus-button': {
        click: this.handleCounterMinusClick.bind(this),
      },
    };
  }

  /**
   * Example of events
   */
  handleCounterPlusClick() {
    const counter = this.model.get('counter');
    this.model.set('counter', counter + 1);
  }

  /**
   * Example of events
   */
  handleCounterMinusClick() {
    const counter = this.model.get('counter');
    this.model.set('counter', counter - 1);
  }

  /**
   * Examples of model
   * @param {*} oldValue 
   * @param {*} newValue 
   */
  handleCounterChange(oldValue, newValue) {
    console.log('Model counter changed', oldValue, ' to ', newValue);
  }
}

VanillaFramework.Application.render('#app', new CounterView({ counter: 0 }));

// test destroying
window.destroyAll = function () {
  VanillaFramework.Application.destroy();
};