class VanillaApplication {
  static region;

  static render(selector, view) {
    VanillaApplication.region = new VanillaFramework.Modules.Region(document, selector);
    VanillaApplication.region.render(view);
  }

  static destroy() {
    if (typeof (VanillaApplication.region) === 'object') {
      VanillaApplication.region.destroy();
    }
  }
}

window.VanillaFramework.Application = VanillaApplication;