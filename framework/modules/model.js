if (typeof (VanillaFramework.Modules.Signal) === 'undefined') {
  throw new Error('Signal is a required module for Model');
}

class ModuleModel {
  constructor(options = {}) {
    this._attributes = Object.assign({}, options);
    this._signals = new VanillaFramework.Modules.Signal();
  }

  get attributes() {
    return Object.assign({}, this._attributes);
  }

  on(property, callback) {
    this._signals.add(property, callback);
  }

  set(property, value) {
    const oldValue = this._attributes[property];
    this._attributes[property] = value;

    // dont match?
    if (!VanillaFramework.Utilities.isEqual(oldValue, value)) {
      this._signals.fire(`change:${property}`, oldValue, value);
    }
  }

  get(property) {
    return this._attributes[property];
  }

  remove(property) {
    if (typeof (this._attributes[property]) !== 'undefined') {
      const oldValue = this._attributes[property];
      delete this._attributes[property];

      // let signals know we deleted a property
      this._signals.fire(`delete:${property}`, oldValue);
    }
  }

  destroy() {
    delete this._attributes;

    this._signals.destroy();
    delete this._signals;
  }
}

VanillaFramework.Modules.Model = ModuleModel;