class ModuleRegion {
  constructor(parentElement, region) {
    this._el = parentElement.querySelector(region);
    this._view = undefined;
  }

  get view() {
    return this._view;
  }

  get el() {
    return this._el;
  }

  render(view) {
    this._view = view;
    this._view.render.call(this._view, this._el);
  }

  refresh() {
    this._view.refresh.call(this._view);
  }

  destroy() {
    this._view.destroy.call(this._view);
    delete this._el;
    delete this._view;
  }
}

VanillaFramework.Modules.Region = ModuleRegion;
