class ModuleSignal {
  constructor() {
    this.channels = {};
  }

  add(channel, callback) {
    if (typeof (this.channels[channel]) === 'object') {
      this.channels[channel].callbacks.push(callback);
      return;
    }

    this.channels[channel] = {
      callbacks: [callback],
    };
  }

  remove(channel, callback) {
    if (typeof (this.channels[channel]) === 'undefined') {
      console.warn(`Could not find channel ${channel}`);
      return;
    }

    this.channels[channel].callbacks = this.channels[channel].callbacks.filter(i => i !== callback);
  }

  fire(channel, ...args) {
    if (typeof (this.channels[channel]) === 'object') {
      this.channels[channel].callbacks.forEach(callback => callback.apply(null, args));
    }
  }

  destroy() {
    delete this.channels;
  }
}

VanillaFramework.Modules.Signal = ModuleSignal;
