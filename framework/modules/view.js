if (typeof (VanillaFramework.Modules.Model) === 'undefined') {
  throw new Error('Model is a required module for View');
}

class ModuleView {
  constructor(options) {
    this._id = VanillaFramework.Utilities.getUUID();
    this._model = new VanillaFramework.Modules.Model(options);
    this._bindings = {};
  }

  get model() {
    return this._model;
  }

  get name() {
    return this._name;
  }

  get el() {
    return this._el;
  }

  get tag() {
    return 'div';
  }

  /**
   * Gets the parent associated view. `undefined` if not available.
   */
  get parent() {
    return this._parent;
  }

  /**
   * See what bindings have been applied from the template.
   */
  get bindings() {
    return this._bindings;
  }

  /**
   * Renders this element
   * @param {*} parentElement 
   */
  render(parentElement) {
    if (typeof (this.template) !== 'string') {
      throw new Error(`View ${this._name} has no valid string template`);
    }

    // call before any renders
    if (typeof (this.onBeforeRender) === 'function') {
      this.onBeforeRender.call(this);
    }

    this._el = document.createElement(this.tag);
    this._el.setAttribute('data-uuid', this._id);
    this._el.innerHTML = this.template;
    parentElement.appendChild(this._el);

    // 01. create attributes on the element first
    this.createAttributes();

    // 02. append bindings, do this early so we dont pick up child views.
    this.createBindings();

    // 03. append uis, do this early so we dont pick up child views.
    this.createUI();

    // 04. append events, do this early so we dont pick up child views.
    this.createEvents();

    // 05. create the regions, so we dont end up picking up any future child events
    this.createRegions();

    // 06. refresh the model so we can see the attributes
    this.refresh();

    // 07. when done, let the view know if it wants todo some extras
    if (typeof (this.onAfterRender) === 'function') {
      this.onAfterRender.call(this);
    }
  }

  /**
   * Refresh is used to basically update the DOM elements and it child views. This will only be called
   * if manually enforced, or the bindings have updated. 
   */
  refresh() {
    Object.keys(this._bindings).forEach((binding) => {
      this._bindings[binding].elements.forEach((element) => {
        switch (element.tagName) {
          case 'INPUT':
            element.value = this.model.get(binding);
            break;
          default:
            element.innerText = this.model.get(binding);
            break;
        }
      });
    });
  }

  /**
   * Creates all the attributes to append onto the tag.
   */
  createAttributes() {
    if (typeof (this.attributes) === 'object') {
      Object.keys(this.attributes).forEach((key) => {
        this._el.setAttribute(key, this.attributes[key]);
      });
    }
  }

  /**
   * Creates all the UI elements so you can easily access the elements within the HTML
   */
  createUI() {
    if (typeof (this.ui) === 'object') {
      Object.keys(this.ui).forEach((key) => {
        const selector = this.ui[key];
        this.ui[key] = this._el.querySelector(selector);
      });
    }
  }

  /**
   * Creates all the events and their listeners
   */
  createEvents() {
    if (typeof (this.events) === 'object') {
      Object.keys(this.events).forEach((key) => {
        const events = Object.keys(this.events[key]);
        events.forEach((event) => {
          this._el.querySelector(key).addEventListener(event, this.events[key][event], false);
        });
      });
    }
  }

  /**
   * Creates all the bindings and hooks up a signal to each one so that they can respond
   * to the change event on a model.
   */
  createBindings() {
    const bindings = this._el.querySelectorAll('[data-bind]');
    bindings.forEach((binding) => {
      const selector = binding.getAttribute('data-bind');
      binding.setAttribute('data-bound', true);
      this.model.on(`change:${selector}`, this.refresh.bind(this));

      this._bindings[selector] = this._bindings[selector] || { elements: [] };
      this._bindings[selector].elements.push(binding);
    });
  }

  /**
   * Creates all the regions and their elements to refresh
   */
  createRegions() {
    if (typeof (this.regions) === 'object') {
      Object.keys(this.regions).forEach((key) => {
        const selector = this.regions[key];
        this.regions[key] = new window.VanillaFramework.Modules.Region(this._el, selector);
      });
    }
  }

  /**
   * Destroy this view which includes, bindings, events, regions etc.
   */
  destroy() {
    this.destroyBindings();
    this.destroyUI();
    this.destroyEvents();
    this.destroyRegions();
    this.model.destroy();
  }

  destroyBindings() {
    delete this._bindings;
  }

  destroyUI() {
    if (typeof (this.ui) === 'object') {
      Object.keys(this.ui).forEach((key) => {
        delete this.ui[key];
      });
    }
  }

  destroyEvents() {
    if (typeof (this.events) === 'object') {
      Object.keys(this.events).forEach((key) => {
        const events = Object.keys(this.events[key]);
        events.forEach((event) => {
          this._el.querySelector(key).removeEventListener(event, this.events[key][event], false);
        });
      });
    }
  }

  destroyRegions() {
    if (typeof (this.regions) === 'object') {
      Object.keys(this.regions).forEach((key) => {
        this.regions[key].delete.call(this.regions[key]);
        delete this.regions[key];
      });
    }
  }

  /**
   * Helper function to get placeholders
   * @param {*} text 
   */
  getModelPlaceholders(text) {
    return text.match(/{([^}]*)}/gi);
  }
}

VanillaFramework.Modules.View = ModuleView;