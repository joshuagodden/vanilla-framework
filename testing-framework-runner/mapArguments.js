/**
 * Default arguments if required
 */
export const DEFAULT_ARGS = {
  testsPath: '',
};

/**
 * Maps the process arguments against the default process arguments.
 * @param {*} processArgs 
 */
export const mapArguments = function (processArgs) {
  const newArguments = Object.assign({}, DEFAULT_ARGS);

  processArgs.forEach((processArg) => {
    if (processArg.startsWith('--')) {
      const [key, value] = processArg.substr(2).split('=');
      if (typeof (newArguments[key]) !== 'undefined') {
        newArguments[key] = isFinite(value) ? parseInt(value) : value;
      }
    }
  });

  return newArguments;
};

export default mapArguments;
