import { mapArguments } from './mapArguments.js';
import { findTests } from './findTests.js';

// get all our arguments ready for running
const testArguments = mapArguments(process.argv);

// get all our tests together
const testFiles = findTests(testArguments.testsPath);

// get all our test modules
const testModules = await Promise.all(testFiles.map(testFile => import(`file:\/\/${testFile.path}`)));

// store all test results here
const testResults = [];

// now run the tests...
testModules.forEach((testModule) => {
  try {
    // check we have a name for this test module...
    const testSuite = testModule.name;
    if (typeof (testSuite) !== 'string') {
      throw new Error('No valid name for the test suite.');
    }

    // check we actually have test cases...
    const testCases = testModule.tests;
    if (typeof (testCases) !== 'object' || !Array.isArray(testCases)) {
      throw new Error('No valid test cases were found.');
    }

    // call any pres/inits etc...
    if (typeof (testModule.beforeAll) === 'function') {
      testModule.beforeAll.call(testModule.beforeAll);
    }

    testCases.forEach((testCase) => {
      if (typeof (testCase.name) !== 'string') {
        throw new Error('Found a test case but no valid name.');
      }

      if (typeof (testCase.test) !== 'function') {
        throw new Error(`${testCase.name} has no valid 'test' function.`);
      }

      // call any pres...
      if (typeof (testCase.beforeEach) === 'function') {
        testCase.beforeEach.call(testCase.beforeEach);
      }

      // get our result (and check its valid...);
      const { expected, received, result } = testCase.test.call(testCase.test);
      if (typeof (result) !== 'boolean') {
        throw new Error(`Expected ${testCase.name} to return a boolean result. Found ${typeof (result)} instead.`);
      }

      // call any posts...
      if (typeof (testCase.afterEach) === 'function') {
        testCase.afterEach.call(testCase.afterEach);
      }

      testResults.push({
        suite: testSuite,
        case: testCase.name,
        expected,
        received,
        result,
      });
    });

    // call any posts/destroys etc...
    if (typeof (testModule.afterAll) === 'function') {
      testModule.afterAll.call(testModule.afterAll);
    }
  } catch (e) {
    console.warn(e.message);
  }

  console.groupEnd();
});

console.table(testResults);