import fs from 'fs';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';

/**
 * Since we are using ESModules, we need to map this like so.
 * @see https://nodejs.org/api/esm.html#esm_no_require_exports_module_exports_filename_dirname
 */
const __filename = fileURLToPath(import.meta.url);

/**
 * Since we are using ESModules, we need to map this like so.
 * @see https://nodejs.org/api/esm.html#esm_no_require_exports_module_exports_filename_dirname
 */
const __dirname = dirname(__filename);

/**
 * Recursively finds a set of tests. If it hits a directory it will call on the next level using this
 * function. If it finds a file that matches *.spec.js then we add that to useable tests.
 * @param {*} previousPath    The previous path (or starting path)
 * @param {*} currentPath     The current path
 */
const recursiveFind = function (previousPath, currentPath, currentTests) {
  const newPath = path.join(previousPath, currentPath);
  fs.readdirSync(newPath, { encoding: 'utf-8', withFileTypes: true }).forEach((file) => {
    const { name } = file;

    if (file.isDirectory() === true) {
      recursiveFind(newPath, name, currentTests);
      return;
    }

    if (name.toLowerCase().endsWith('.spec.js')) {
      currentTests.push({ name, path: path.join(newPath, name) });
    }
  });
};

/**
 * Finds all the SPEC tests available for running.
 * @param {*} startPath   The starting path for finding the tests.
 */
export const findTests = function (startPath) {
  const tests = [];
  const basePath = path.join(__dirname, '../');
  
  // change this to be functional
  recursiveFind(basePath, startPath, tests);
  return tests;
};

export default findTests;