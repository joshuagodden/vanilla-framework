import { expect } from './index.js';

export const name = 'testing-framework';
export const tests = [
  {
    name: 'toEqual works with number',
    test: function() {
      return expect(1).toEqual(1);
    },
  },
  {
    name: 'toEqual works with string',
    test: function() {
      return expect('foo').toEqual('foo');
    },
  },
  {
    name: 'toEqual works with undefined',
    test: function() {
      return expect(undefined).toEqual(undefined);
    },
  },
  {
    name: 'toEqual works with null',
    test: function() {
      return expect(null).toEqual(null);
    },
  },
  {
    name: 'toEqual works with object',
    test: function() {
      return expect({ a: 1 }).toEqual({ a: 1 });
    },
  },
  {
    name: 'toEqual works with arrays',
    test: function() {
      return expect([{ a: 1 }]).toEqual([{ a: 1 }]);
    },
  },
];

export default { name, tests };
