import { equals, arrayEquals } from '../utils/equals.js';

class VanillaTest {
  constructor(expected) {
    this._expected = expected;
    this._received = undefined;
    this._result = false;
  }

  get expected() {
    return this._expected;
  }

  get received() {
    return this._received;
  }

  get result() {
    return this._result;
  }

  static expect(value) {
    return new VanillaTest(value);
  }

  /**
   * Strict equal checks
   * @param {*} received 
   */
  toEqual(received) {
    this._received = received;

    if ((typeof (this.expected) === 'object' && Array.isArray(this.expected))
      && (typeof (this.received) === 'object' && Array.isArray(this.received))) {
        this._result = arrayEquals(this.expected, received);
    } else {
      this._result = equals(this.expected, received);
    }


    return this.getResult();
  }

  /**
   * Checks if the value is some form of true
   * @param {*} received 
   */
  toBeTruthy(received) {
    this._received = received;
    this._result = received == true;
    return this.getResult();
  }

  /**
   * Checks if the value is some form of false
   * @param {*} received 
   */
  toBeFalsy(received) {
    this._received = received;
    this._result = received == false;
    return this.getResult();
  }

  /**
   * Get the result
   */
  getResult() {
    return {
      expected: this.expected,
      received: this.received,
      result: this.result,
    };
  }
}

export const { expect } = VanillaTest;
export default VanillaTest;
